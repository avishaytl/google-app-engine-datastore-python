import datetime 
import json
from flask import Flask, request, render_template
from google.cloud import datastore   

app = Flask(__name__)
datastore_client = datastore.Client() 

def store_time(dt):
    entity = datastore.Entity(key=datastore_client.key('visit'))
    entity.update({
        'timestamp': dt
    }) 
    datastore_client.put(entity) 

def fetch_times(limit):
    query = datastore_client.query(kind='visit')
    query.order = ['-timestamp'] 
    times = query.fetch(limit=limit) 
    return times 
    
def setNewItem(req): 
    entity = datastore.Entity(key=datastore_client.key(req['token']))
    entity.update({ 
        'key': req['item'],
        'img': req['img'] ,
        'name': req['name'],
        'howto': req['howto'] 
    }) 
    datastore_client.put(entity) 
     
def getItems(req): 
    query = datastore_client.query(kind=req['token'])  
    query.order = [] 
    res = query.fetch()
    return res

def deleteItems(req):  
    query = datastore_client.query(kind=req['token'])   
    res = query.fetch() 
    for entity in res: 
        datastore_client.delete(entity.key)  

@app.route('/')
def root():   
    now = datetime.datetime.now()    
    store_time(datetime.datetime.now()) 
    times = fetch_times(10) 
    return render_template('index.html', times=times)

@app.route('/getReadListItems')
def getReadListItems(): 
    token = request.args['token']  
    res = list(getItems({'token': token}))
    if len(res):
        return json.dumps([{'success': True, 'list': res}]) 
    return json.dumps([{'success': True, 'msg': 'Cant Find Read List'}]) 
    
@app.route('/setUserToken')
def setUserToken(): 
    token = request.args['token'] 
    entity = datastore.Entity(key=datastore_client.key(token)) 
    datastore_client.put(entity)
    return json.dumps([{'success': True, 'token': token}])
    
    
@app.route('/addNewItemToList')
def addNewItemToList(): 
    token = request.args['token']  
    item = request.args['item']  
    img = request.args['img']  
    name = request.args['name'] 
    howto = request.args['howto']  
    setNewItem({'token': token, 'item': item, 'img': img, 'name': name, 'howto': howto})
    return json.dumps([{'success': True, 'token': token}])
    
@app.route('/deleteItemFromList')
def deleteItemFromList(): 
    token = request.args['token']  
    item = request.args['item']  
    res = list(getItems({'token': token}))
    deleteItems({'token': token})
    if len(res) > 1:
        for entity in res: 
            if entity['key'] != item:
                setNewItem({'token': token, 'item': entity['key'], 'img': entity['img'], 'name': entity['name'], 'howto': entity['howto']}) 
    return json.dumps([{'success': True, 'token': token}])

if __name__ == '__main__': 
    app.run(host='127.0.0.1', port=8080, debug=True) 
