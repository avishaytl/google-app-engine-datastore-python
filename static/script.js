'use strict';

window.addEventListener('load', function () {

  const url = 'https://fastsimon-321916.appspot.com'; // # https://fastsimon-321916.appspot.com # http://localhost:8080
  const usertoken = '@token10';
  setItems();  

  var modal = document.getElementById("myModal"); 
  var btn = document.getElementById("myBtn"); 
  var span = document.getElementsByClassName("close")[0]; 
  btn.onclick = function() {
    modal.style.display = "block";
    getReadListItems(usertoken.replace(':','-')) 
  } 
  span.onclick = function() {
    modal.style.display = "none";
  } 
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  } 

  function setItems() { 
    var options = {
      method: 'GET',  
    }; 
    fetch('https://www.thecocktaildb.com/api/json/v1/1/search.php?s=all',options).then((response) => { 
            return response.json().then((data) => {
                console.log('setItems',data);
                let drinks = [];
                for(let i = 0;i < data['drinks'].length;i++)
                  drinks.push({
                    key: data['drinks'][i]['idDrink'], 
                    img: data['drinks'][i]['strDrinkThumb'],
                    name: data['drinks'][i]['strDrink'],
                    info: getItemInfo(data['drinks'][i]),
                    howto: data['drinks'][i]['strInstructions']
                  }) 
                let x = document.getElementById("ma-container"); 
                let html = ``;
                for(let i = 0;i < drinks.length;i++){ 
                  html += `<div class="item">
                              <p id="${drinks[i].key}-name"><b>${drinks[i].name}</b></p>
                              <p id="${drinks[i].key}">Item ID: <b>${drinks[i].key}</b></p>
                              <img id="${drinks[i].key}-img" src="${drinks[i].img}" alt="item ${drinks[i].key}"/> 
                              <p class="item-info">${drinks[i].info}</p>
                              <p class="item-howto" id="${drinks[i].key}-howto">${drinks[i].howto}</p>
                              <div onClick="addReadItem(${drinks[i].key})" class="item-btn"><i id="${drinks[i].key}-i">add to Read List</i></div>
                           </div>`;
                }
                x.innerHTML = html; 
                setSelectedItemList(drinks)
                return data;
            }).catch((err) => {
                console.log(err);
            }) 
        }).catch(function (error) {
      console.error(error);
    }); 
  }

  function setSelectedItemList(items){
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", "Basic Og==");
    let options = {
      method: 'GET',  
      headers: myHeaders,
      redirect: 'follow'
    }; 
    fetch(url + '/getReadListItems?token=' + usertoken,options).then((response) => { 
            return response.json().then((data) => {
                console.log('setSelectedItemList',data);  
                for(let i = 0;i < data[0]['list'].length;i++){ 
                  for(let j = 0; j < items.length;j++)
                    if(data[0]['list'][i]['key'] === items[j]['key']){  
                      let idTitle = document.getElementById(data[0]['list'][i]['key']); 
                      let readTitle = document.getElementById(`${data[0]['list'][i]['key']}-i`);
                      idTitle.style.color = 'tomato'; 
                      readTitle.innerHTML = 'remove from Read List';
                      readTitle.style.color = '#2d6e2d' 
                    } 
                }  
                return data;
            }).catch((err) => {
                console.log(err);
            }) 
        }).catch(function (error) {
      console.error(error);
    }); 
    
  } 
  function getReadListItems(token) { 
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", "Basic Og==");
    let options = {
      method: 'GET',  
      headers: myHeaders,
      redirect: 'follow'
    }; 
    fetch(url + '/getReadListItems?token=' + token,options).then((response) => { 
            return response.json().then((data) => {
                console.log('getReadListItems',data); 
                var view = document.getElementById("modal-content-view"); 
                let html = ``;
                if(data[0] && data[0]['list'])
                  for(let i = 0;i < data[0]['list'].length;i++){ 
                    html += `<div onClick="alert('${data[0]['list'][i]['howto']}')" class="modal-item"> 
                                <img src="${data[0]['list'][i]['img']}" alt="item ${data[0]['list'][i]['key']}"/> 
                                <p>Cocktail: <b>${data[0]['list'][i]['name']}</b></p>
                                <p id="${data[0]['list'][i]['key']}">ID: <b>${data[0]['list'][i]['key']}</b></p> 
                            </div>`;
                  } 
                else html = `Read List Is Empty...`
                view.innerHTML = html;
                return data;
            }).catch((err) => {
                console.log(err);
            }) 
        }).catch(function (error) {
      console.error(error);
    }); 
  }  
});
